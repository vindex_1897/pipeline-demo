module.exports = {
    BUILD_DIR: "build",
    UPLOAD_DIR_NAME: "tmp",
    DEPLOY_DIR_NAME: "public",
    DEPLOY_PATH: "/var/www/html/react-app",
};
