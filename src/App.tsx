import * as React from "react";

import PageInterface from "./PageInterface";

class App extends React.Component<PageInterface, {}> {
    render() {
        return (
            <div style={{ backgroundColor: this.props.color }}>
                <h1>Welcome to ReactJS with Typescript</h1>
                <p>The color of this page is {this.props.color}</p>
                <p>This page is created with ReactJS</p>
                <p>This is a new commit :D</p>
            </div>
        );
    }
}

export default App;
